package ch.hefr;

import ch.hefr.drawable.Circle;
import ch.hefr.drawable.Line;
import ch.hefr.drawable.CompositeDrawable;
import ch.hefr.drawable.Rectangle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Drawing Operations Test");
        Parent root = FXMLLoader.load(getClass().getResource("canvas.fxml"));
        Canvas canvas = (Canvas) root.lookup("#canvas");
        GraphicsContext gc = canvas.getGraphicsContext2D();
        drawStickmen(gc);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private void drawStickmen(GraphicsContext gc) {
        Circle skull = new Circle(100, 0, 50);
        Circle leftEye = new Circle(107.5, 20, 10);
        Circle rightEye = new Circle(132.5, 20, 10);
        Circle mouth = new Circle(120, 35, 10);

        Rectangle trunk = new Rectangle(100, 50, 50, 100);

        Line rightArm = new Line(150, 50, 200, 0);
        Line leftArm = new Line(100, 50, 50, 100);

        Line rightLeg = new Line(150, 150, 225, 175);
        Line leftLeg = new Line(100, 150, 25, 175);

        CompositeDrawable head = new CompositeDrawable();
        head.add(skull);
        head.add(leftEye);
        head.add(rightEye);
        head.add(mouth);

        CompositeDrawable arms = new CompositeDrawable();
        arms.add(leftArm);
        arms.add(rightArm);

        CompositeDrawable legs = new CompositeDrawable();
        legs.add(leftLeg);
        legs.add(rightLeg);

        CompositeDrawable body = new CompositeDrawable();
        body.add(trunk);
        body.add(arms);
        body.add(legs);

        CompositeDrawable stickman = new CompositeDrawable();
        stickman.add(head);
        stickman.add(body);

        stickman.draw(gc);

        stickman.moveBy(200, 0);
        stickman.setColor(Color.RED);
        stickman.draw(gc);

        stickman.moveBy(-200, 175);
        head.setColor(Color.CYAN);
        body.setColor(Color.BLUE);
        stickman.draw(gc);

        stickman.moveBy(200, 0);
        stickman.setColor(Color.BLACK);
        head.setColor(Color.ORANGE);
        mouth.setColor(Color.PINK);
        body.setColor(Color.CHOCOLATE);
        legs.setColor(Color.BLACK);
        stickman.draw(gc);
    }
}
