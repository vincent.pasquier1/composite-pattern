package ch.hefr.drawable;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public interface Drawable {
    void draw(GraphicsContext gc);
    void setColor(Color color);
    void moveBy(double x, double y);
}
