package ch.hefr.drawable;

import javafx.scene.canvas.GraphicsContext;

public class Line extends AbstractDrawable {

    private double dx, dy;

    public Line(double ax, double ay, double bx, double by) {
        super(ax, ay);
        dx = bx - ax;
        dy = by - ay;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.strokeLine(x, y, x + dx, y + dy);
    }
}
