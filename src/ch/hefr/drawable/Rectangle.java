package ch.hefr.drawable;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.ArcType;

public class Rectangle extends AbstractDrawable {

    private double w, h;

    public Rectangle(double x, double y, double width, double height) {
        super(x, y);
        w = width;
        h = height;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.strokeRect(x, y, w, h);
    }
}
