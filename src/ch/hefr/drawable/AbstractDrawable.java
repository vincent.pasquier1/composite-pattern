package ch.hefr.drawable;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class AbstractDrawable implements Drawable {

    protected Color color;
    protected double x, y;

    AbstractDrawable(double x, double y) {
        this.x = x;
        this.y = y;
        this.color = Color.BLACK;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void moveBy(double x, double y) {
        this.x += x;
        this.y += y;
    }
}
